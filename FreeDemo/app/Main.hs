module Main (main) where

-- import qualified Lists
import Data.Function
import qualified Threads

data SL a = Empty | SL a :> a

instance Monoid (SL a) where
  mempty = Empty
  mappend ys Empty = ys
  mappend ys (xs :> x) = (mappend ys xs) :> x

instance Semigroup (SL a) where
  (<>) = mappend

single :: a -> SL a
single x = Empty :> x

-- x :: SL Int -> SL Int
-- x = (single 1 <>)

fx :: SL Int
fx = (single 1 <>) fx

test :: Int
test =
  case fx of
    _ :> _ :> _:> _ -> 1
    _ -> 2

main :: IO ()
main = do
  -- Lists.main
  Threads.main
  -- print test -- hangs
  return ()
