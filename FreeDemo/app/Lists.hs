{-# LANGUAGE ScopedTypeVariables #-}
module Lists (main) where

import Control.Monad.Free

demo1 :: [Integer]
demo1 = do
  b <- [False,True]  -- not of the form (return _)
  if b
      then return 47
      else []

-- demo2 :: Free ((,)Integer) ()
-- demo2 = do
--   (b :: Bool) <- Free (False, Free (True, Pure ()))  -- not of the form (return _)
--   if b
--       then return $ Free (47, Pure ())
--       else Pure ()

main :: IO ()
main = do
  print demo1
  -- print demo2
  return ()
