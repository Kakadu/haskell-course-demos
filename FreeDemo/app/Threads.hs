{-# LANGAUGE DerivingFunctor #-}
{-# LANGAUGE NoImplicitPrelude #-}
module Threads (main) where

import Control.Monad.Free (liftF, Free(..))
import Control.Exception (throwIO)
import Control.Monad (liftM)

data Toy b next =
    Output b next
  | Bell next
  | Done

instance Functor (Toy b) where
  fmap f (Output x next) = Output x (f next)
  fmap f (Bell next) = Bell (f next)
  fmap f Done = Done

bell :: Free (Toy a) ()
bell = Free (Bell (Pure ()))

done :: Free (Toy a) r
done = Free Done

-- liftF :: (Functor f) => f r -> Free f r
-- liftF command = Free (fmap Pure command)

make1 :: x -> Toy x ()
make1 x = Output x ()

output :: a -> Free (Toy a) ()
output x = liftF (Output x ())

catch :: (Functor f) => Free f e1 -> (e1 -> Free f e2) -> Free f e2
catch (Free x) f = Free (fmap (flip catch f) x)
catch (Pure e) f = f e

subroutine :: Free (Toy Char) ()
subroutine = output 'A'

program :: Free (Toy Char) r
program = do
  subroutine
  bell
  done

showProgram :: (Show a, Show r) => Free (Toy a) r -> String
showProgram (Free (Bell x))     = "bell\n" ++ showProgram x
showProgram (Free Done)         = "done\n"
showProgram (Pure r)            = "return " ++ show r ++ "\n"
showProgram (Free (Output a x)) = "output " ++ show a ++ "\n" ++ showProgram x

pretty :: (Show a, Show r) => Free (Toy a) r -> IO ()
pretty = putStr . showProgram

testPretty = do
  pretty (program :: Free (Toy Char) ())
  pretty ((output 'A' >> done) >> output 'C')


{- ringing a bell -}
-- interpret :: (Show b) => Free (Toy b) r -> IO ()
-- interpret (Free (Output b x)) = print b >> interpret
-- interpret (Free (Bell x)) = ringBell >> interpret
-- interpret (Free Done) = return ()
-- interpret (Pure r) = throwIO (userError "Unexpected termination")

-- ---------------------------------------------------------------------
{- threads -}
-- data Thread m r = Atomic (m (Thread m r)) | Return r
-- is the same as Free
atomic m = Free (liftM Pure m)

thread1 :: Free IO ()
thread1 = do
  atomic (print 1)
  atomic (print 2)
thread2 :: Free IO ()
thread2 = do
  atomic (putStrLn "Enter something:")
  str <- atomic getLine
  atomic (putStrLn $ "You entered: " ++ str)

interleave :: (Monad m) => Free m r -> Free m r -> Free m r
interleave t1 (Pure _) = t1
interleave (Pure _) t2 = t2
interleave (Free m1) (Free m2) = do
  next1 <- atomic m1
  next2 <- atomic m2
  interleave next1 next2

runThread :: (Monad m) => Free m r -> m r
runThread (Free m) = m >>= runThread
runThread (Pure r) = return r

testThread :: IO ()
testThread = runThread (interleave thread1 thread2)
{- ---------------------------------------------------------------- -}
main :: IO ()
main = do
  testPretty
  testThread
  return ()
