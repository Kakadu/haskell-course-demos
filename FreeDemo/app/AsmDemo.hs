{-# LANGUAGE DeriveFunctor #-}
module AsmDemo where

import Data.Word
import Data.Bits (shiftR)

newtype ASM6502 a = ASM6502 (Word16 -> ([Word8], Word16, a))
  deriving Functor

-- Retrieve the inner function
asmFunction (ASM6502 f) = f
-- Evaluate the monad and get a list of bytes
asm start (ASM6502 f) = bytes where (bytes, _, _) = f start

instance Monad ASM6502 where
  return ret = ASM6502 $ \loc -> ([], loc, ret)
  a >>= b = ASM6502 $ \start -> let
      (left, mid, val) = asmFunction a start
      (right, end, ret) = asmFunction (b val) mid
      in (left ++ right, end, ret)

instance Applicative ASM6502 where
  pure = return
  (<*>) f x = f >>= \f -> x >>= \x -> return $ f x


byte :: Word8 -> ASM6502 ()
byte x = ASM6502 $ \loc -> ([x], loc + 1, ())

ldai x = byte 0xa9 >> byte x  -- load immediate value into A register
clc = byte 0x18  -- clear carry flag
adci x = byte 0x69 >> byte x  -- A += x, with carry
cmpi x = byte 0xc9 >> byte x  -- compare A to x, setting status flags

here :: ASM6502 Word16
here = ASM6502 $ \loc -> ([], loc, loc)

bne :: Word16 -> ASM6502 ()  -- branch if not equal
bne label = ASM6502 $ \loc -> let
    rel = toInteger label - toInteger (loc + 2)
    rel8 = if rel > 127 || rel < -128
        then error "label given to bne is too far away"
        else fromInteger rel
    in ([0xf0, rel8], loc + 2, ())

jmp :: Word16 -> ASM6502 ()
jmp label = do
    byte 0x4c
    byte (fromIntegral label)  -- Little end first
    byte (fromIntegral (label `shiftR` 8))  -- shiftR is defined in Data.Bits


program :: [Word8]
program = asm 0x8000 $ do
    ldai 0
    clc
    incloop <- here
    adci 1
    cmpi 0xf0
    bne incloop
    forever <- here
    jmp forever
