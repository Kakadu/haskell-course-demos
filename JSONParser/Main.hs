{-# LANGUAGE QuasiQuotes   #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import NeatInterpolation (text)
import JSON
import Text.Megaparsec hiding (parseTest)
import Text.PrettyPrint.GenericPretty

input1 = [text|
{
  "first name": "John",
  "last name": "Smith",
  "age": 25,
  "address": {
    "street address": "21 2nd Street",
    "city": "New York",
    "state": "NY",
    "postal code": "10021"
  },
  "phone numbers": [
    {
      "type": "home",
      "number": "212 555-1234"
    },
    {
      "type": "fax",
      "number": "646 555-4567"
    }
  ],
  "sex": {
    "type": "male"
  }
}
|]

parseTest ::  ( ShowErrorComponent e
              , Out a
              , Stream s
              ) =>
              Parsec e s a -> s -> IO ()
parseTest p input =
  case parse p "" input of
    Left  e -> putStr (errorBundlePretty e)
    Right x -> pp x

main = do
  print $ JSON.parseJSON input1
  parseTest JSON.json input1
  return ()
