{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ExplicitForAll, ScopedTypeVariables, RankNTypes #-}

-- https://markkarpov.com/megaparsec/megaparsec.html
-- megaparsec tutorial
module Main (main) where

import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Text (Text)
import Data.Void
-- import Control.Monad
-- import Data.Functor.Identity (Identity)

type Parser = Parsec Void Text

data Expr = Num Char
          | Mul Expr Expr
          | Add Expr Expr
    deriving Show

makeop :: (Expr -> Expr -> Expr) -> (Expr, [Expr]) -> Expr
makeop op (x,xs) = foldl op x xs
-- makemul = makeop Mul
-- makeAdd = makeop Add


-- implementing parser with priority here
arith :: Parser Expr -> Parser Expr
arith item = root
  where
    (<<) :: (Monad m) => forall . m a -> m b -> m a
    (<<) l r = l >>= \ans -> r >>= \_ -> return ans

    inparens :: forall a .  Parser a -> Parser a
    inparens p = (char '(' >> p << char ')')

    root :: Parser Expr
    root = sums

    sums :: Parser Expr
    sums = makeop Add <$>
      pair  (muls <|> item)
            (many (char '+' >> (muls <|> item) ) )
    muls :: Parser Expr
    muls = makeop Mul <$>
      pair  (item <|> (inparens root))
            (many (char '*' >> (item <|> (inparens root)) ) )

    pair :: Monad m => forall a b. m a -> m b -> m (a,b)
    pair p1 p2 = p1 >>= \a -> p2 >>= \b -> return (a,b)

digit ::  Parser Expr
digit = Num <$> (oneOf ['0'..'9'] <?> "digit")

main :: IO ()
main = do
  parseTest (arith digit) "2+3+4"
  putStrLn ""
  parseTest (arith digit) "2*3"
  putStrLn ""
  parseTest (arith digit) "1+2*3"
  putStrLn ""
  parseTest (arith digit) "1+(2*3)"
  putStrLn ""
  parseTest (arith digit) "2*3+1+(2*3)"
  putStrLn ""
  parseTest (arith digit) "2*(1+3)"
