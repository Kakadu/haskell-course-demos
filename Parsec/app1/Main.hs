{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ExplicitForAll, ScopedTypeVariables, RankNTypes #-}

-- https://markkarpov.com/megaparsec/megaparsec.html
-- megaparsec tutorial
module Main (main) where

import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Text (Text)
import Data.Void

type Parser = Parsec Void Text

main :: IO ()
main = do
  parseTest (char 'a' :: Parser Char) "b"
  putStrLn ""
  parseTest (many (char 'a' :: Parser Char) ) "b"
  putStrLn ""
  parseTest (many (char 'a' :: Parser Char) ) "aaaab"
