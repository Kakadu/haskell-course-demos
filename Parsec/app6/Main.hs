{-# LANGUAGE LambdaCase, InstanceSigs #-}
-- {-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE ExplicitForAll, ScopedTypeVariables, RankNTypes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

-- code for parser combinator tutorial
-- http://www.cs.uu.nl/research/techreps/repo/CS-2008/2008-044.pdf
module Main (main) where

-- import NeatInterpolation (text)
import Control.Applicative
import Control.Monad (replicateM)
import Prelude

type Stream s = [s]
newtype Parser s t = P (Stream s -> [(t, Stream s)])

run :: Parser s a -> Stream s -> [(a, Stream s)]
run (P f) = f

-- parser that returns constant value
pReturn :: a -> Parser s a
pReturn x = P $ \s -> [ (x,s) ]

-- apply parser that contains function to another parser
apply :: Parser s (a -> b) -> Parser s a -> Parser s b
apply (P p1) (P p2) =
  P $ \inp ->
      flip concatMap (p1 inp) $ \ (f, tl1) ->
      flip concatMap (p2 tl1) $ \ (x, tl2) ->
      [(f x, tl2)]

apply_using_list_comprehensions :: forall s t t1 .
            Parser s (t -> t1) -> Parser s t -> Parser s t1
apply_using_list_comprehensions (P p1) (P p2) =
  P $ \inp -> [ (v1 v2, ss2) | (v1, ss1) <- p1 inp
                             , (v2, ss2) <- p2 ss1 ]

-- fmap: apply function to every parser result
forEach :: (a -> b) -> Parser s a -> Parser s b
forEach f (P p) =
  P $ \inp -> map (\ (a,tl) -> (f a, tl)) $ p inp

instance Functor (Parser s) where
  fmap :: (a -> b) -> Parser s a -> Parser s b
  fmap = forEach

instance Applicative (Parser s) where
  pure :: a -> Parser s a
  pure = pReturn
  (<*>) :: Parser s (b ->a) -> Parser s b -> Parser s a
  (<*>) = apply
  -- or using monad
  -- p1 <*> p2 = p1 >>= \f -> p2 >>= \x -> return (f x)

pFail :: Parser s a
pFail = P (const [])

alt :: Parser s a -> Parser s a -> Parser s a
alt (P p1) (P p2) = P (\ inp -> p1 inp ++ p2 inp)

-- monoid on applicative parsers
instance Alternative (Parser s) where
  empty :: Parser s a
  empty = pFail
  (<|>) :: Parser s a -> Parser s a -> Parser s a
  (<|>) = alt

instance Monad (Parser s) where
  return :: a -> Parser s a
  return = pReturn

  (>>=) :: Parser s a -> (a -> Parser s b) -> Parser s b
  P pa >>= a2pb = P $ \input ->
    -- [ input2 | (a, input1) <- pa input
    --          , input2 <- run (a2pb a) input1 ]
    flip concatMap (pa input) $ \ (x,tl) ->
    run (a2pb x) tl

-- Applicative vs. Monadic
-- https://stackoverflow.com/questions/7861903/




----------------------------------------------------------

pSym :: Eq s => s -> Parser s s
pSym c = P $ \case
  c2 : tl   | c2 == c -> [(c, tl)]
  _                   -> []

pLettera :: Parser Char Char
pLettera = pSym 'a'

char :: Char -> Parser Char Char
char c = P $ \case
  c2 : tl   | c2 == c -> [(c, tl)]
  _                   -> []



-- Applicative vs. Monadic
-- https://stackoverflow.com/questions/7861903/


-- Applicative style (Stepik)
pString_aa :: Parser Char [Char]
pString_aa =
  pReturn(:)
    <*> pLettera
    <*> (pReturn (\x -> [x]) <*> pLettera)

-- monadic style
pString_bb :: Parser Char [Char]
pString_bb = char 'b' >>= \a -> char 'b' >>= \b -> return [a,b]

-- monadic with do-notation
pString_cc :: Parser Char [Char]
pString_cc = do
  a <- char 'c'
  b <- char 'c'
  return [a, b]

-- I define my custom operators to be in sync with tutorial
-- http://www.cs.uu.nl/research/techreps/repo/CS-2008/2008-044.pdf

opt :: forall s a . Parser s a -> a -> Parser s a
p `opt` v = p <|> pReturn v

pMany :: forall s a . Parser s a -> Parser s [a]
pMany p = pure (:) <*> p <*> opt (pMany p) []

pMany1 :: forall s a . Parser s a -> Parser s [a]
pMany1 p = pure (:) <*> p <*> pMany p

pDigit :: Parser Char Char
pDigit = pSatisfy (\c -> '0'<= c && c <= '9')

pSatisfy :: (s -> Bool) -> Parser s s
pSatisfy cond = P $ \case
  (x:xs) | cond x -> [(x,xs)]
  _               -> []
-- pSym a = pSatisfy (≡a)

pDigitAsInt :: Parser Char Int
pDigitAsInt = (\ c -> fromEnum c - fromEnum '0') <$> pDigit

pNatural :: Parser Char Int
pNatural = foldl (\ a b -> a * 10 + b) 0 <$> pMany1 pDigitAsInt

parseTest :: forall a s . (Show a, Show s) => Parser s a -> Stream s -> IO ()
parseTest p s = print (run p s)

applyN :: Int -> (a -> a) -> a -> a
applyN n f = foldr (.) id (replicate n f)

anbncn :: Parser Char ()
anbncn = do
  xs <- many (pSym 'a')
  _ <- replicateM (length xs) (pSym 'b' >> return ())
  _ <- replicateM (length xs) (pSym 'c' >> return ())
  return ()

main :: IO ()
main = do
  parseTest pString_aa "aaa"
  parseTest pString_bb "bbccc"
  parseTest pString_cc "cccfffff"
  parseTest pNatural   "1123"
  parseTest anbncn     "aaabbbccc"
  return ()
