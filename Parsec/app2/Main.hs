{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ExplicitForAll, ScopedTypeVariables, RankNTypes #-}

-- https://markkarpov.com/megaparsec/megaparsec.html
-- megaparsec tutorial
module Main(main) where

import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Text (Text)
import Data.Void

type Parser = Parsec Void Text

data Expr = Num Char
          | Add Expr Expr
    deriving Show

-- BAD implementation
arith :: Parser Expr -> Parser Expr
arith item = sums
  where
    sums :: Parser Expr
    sums = sums >>= \l -> (char '+') >> sums >>= \r -> return (Add l r)
      <|> item

digit ::  Parser Expr
digit = Num <$> (oneOf ['0'..'9'] <?> "digit")

_hanging1 :: IO ()
-- DON'T RUN IT. IT HANGS!!!!1
_hanging1 = parseTest (arith digit) "2+3+4"

main :: IO ()
main = do
  putStrLn ""
