{-# LANGUAGE NamedFieldPuns, LambdaCase #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
-- {-# OPTIONS_GHC -fsimpl-tick-factor=5000 #-}
{-# OPTIONS_GHC -O0 #-}
module Main where

import Data.List   ( (\\) )
import Text.Printf
import Debug.Trace

type VarName = String
data Lam  = Var VarName
          | App Lam Lam
          | Abs VarName Lam
  deriving Eq

instance Show Lam where
  show (Var s) = s
  show (App l r) = printf "(%s %s)" (show l) (show r)
  show (Abs x (Abs y (Abs z (Abs z2 t)))) = printf "(λ %s %s %s %s -> %s)" x y z z2 (show t)
  show (Abs x (Abs y (Abs z t))) = printf "(λ %s %s %s -> %s)" x y z (show t)
  show (Abs x (Abs y t)) = printf "(λ %s %s -> %s)" x y (show t)
  show (Abs x t) = printf "(λ %s -> %s)" x (show t)

names :: Lam  -> [VarName]
names l = helper [] l
  where
    helper acc (Var s) = s:acc
    helper acc (Abs s l) = helper (s:acc) l
    helper acc (App l r) = helper (helper acc r) l

freeVars :: Lam -> [String]
freeVars l = helper [] l
  where
    helper acc (Var s) = s:acc
    helper acc (Abs s l) = acc ++ ((helper [] l) \\ [s])
    helper acc (App l r) = helper (helper acc r) l

nextName :: String -> [String] -> String
nextName s old = helper s old
  where
    helper s old | elem s old = helper ("_" ++ s) old
    helper s _                = s

replaceName :: VarName -> VarName -> Lam -> Lam
replaceName x a t = ans
  where
    ans  = helper t
    helper (Var y)   | x == y = Var a
    helper (Var t)            = Var t
    helper (App l r)          = App (helper l) (helper r)
    helper (Abs y t) | x == y = Abs a (helper t)
    helper (Abs z t)          = Abs z (helper t)

is_free_in x t = x `elem` (freeVars t)

-- subst (x,v) e means `[x/v]e` or `e[v -> x]`
subst :: (String,Lam) -> Lam -> Lam
subst (x,v) tt =
  let rez = helper tt in
  -- trace (printf "subst %s ↦ %s in `%s` is %s" x (show v) (show tt) (show rez)) $
  rez
  where
    helper e =
      -- trace (printf "\t%s ↦ %s in `%s`" x (show v) (show e)) $
      -- trace (printf "\t   is `%s`" (show ans)) $
        ans
      where
        ans = case e of
          Var y | y==x -> v
          Var y        ->  Var y
          App l r      -> App (helper l) (helper r)
          Abs y b | y == x -> Abs y b
          Abs y t | y `is_free_in` v ->
              helper (Abs w (replaceName y w t))
            where
              w = nextName y frees
              frees = freeVars v ++ freeVars t
          Abs y b      -> Abs y (helper b)


-- Evaluation strategy in open recursion style
data Strat = Strat { onVar :: Strat -> VarName -> Lam
                   , onAbs :: Strat -> VarName -> Lam -> Lam
                   , onApp :: Strat -> Lam -> Lam -> Lam
                   }

appStrat st (Var x)   = (onVar st) st x
appStrat st (Abs x e) = (onAbs st) st x e
appStrat st (App f x) = (onApp st) st f x

{- Implementing extensible strategies -}
-- No evaluation
dummyStrat = Strat var abs app where
  var _  = Var
  abs _  = Abs
  app _  = App

-- Call-by-Name Reduction to Weak Head Normal Form
cbnStrat = dummyStrat { onApp = app } where
  app st f1 arg =
    case appStrat st f1 of
      Abs x e -> appStrat st $ subst (x, arg) e
      f2      -> App f2 arg

-- Normal Order Reduction to Normal Form
-- Application function reduced as CBN first
-- + Reduce under abstractions
norStrat = cbnStrat { onApp, onAbs } where
  onAbs st x b = Abs x $ appStrat st b
  onApp st f0 arg =
    case appStrat cbnStrat f0 of
      Abs x e -> appStrat st $ subst (x, arg) e
      f1      ->
        let f2 = appStrat st f1 in
        let arg2 = appStrat st arg in
        App f2 arg2

-- Call-by-Value Reduction to Weak Normal Form
cbvStrat = dummyStrat { onApp = app } where
  app st f1 arg =
    case appStrat st f1 of
      Abs x e ->  let arg2 = appStrat st arg in
                  appStrat st $ subst (x, arg2) e
      f2      -> App (appStrat st f2) (appStrat st arg)

-- Applicative Order Reduction to Normal Form
-- As CBV but reduce under abstractions
aoStrat = cbvStrat { onAbs = abs } where
  abs st x e = Abs x $ appStrat st e

{-
-- Hybrid Applicative Order Reduction to Normal Form
-- It relates to call-by-value in the
-- same way that the normal order strategy relates to call-by-name.
haStrat = aoStrat { onApp = app } where
  app st f1 arg =
    case appStrat cbvStrat f1 of
      Abs x e -> appStrat st $ subst (x, arg) e
      f2      -> App (appStrat st f2) (appStrat st arg)

--  Head Spine Reduction to Head Normal Form
heStrat = dummyStrat { onAbs = abs, onApp = app } where
  abs st x e = Abs x $ appStrat st e
  app st f1 arg =
    case appStrat st f1 of
      Abs x e -> appStrat st $ subst (x, arg) e
      f2      -> App f2 arg

-- Hybrid Normal Order Reduction to Normal Form
hnStrat = norStrat { onApp = app } where
  app st f1 arg =
    case appStrat heStrat f1 of
      Abs x e -> appStrat st $ subst (x, arg) e
      f2      -> App (appStrat st f2) (appStrat st arg)
-}

cbnBigStepStrat = Strat { onApp , onAbs, onVar }
  where
    onApp _ f x = main (App f x)
    onAbs _ x b = main (Abs x b)
    onVar _ x   = main (Var x)
    main t = case helper t of
      Left  t2 -> trace (printf " -- %s" (show t2)) (main t2)
      Right t2 -> t2
    helper (Var x)     =
        --trace "var" $
        Right $ Var x
    helper (Abs x b)   =
      --trace "abs" $
      Right $ Abs x b
    helper (App f arg) = -- trace "app" $
      case helper f of
        Left f2         -> --trace "1" $
                           Left $ App f2 arg
        Right (Abs x e) -> --traceStack "2" $
                           Left $ subst (x,arg) e
        Right f2        -> -- trace "3" $
                           Right $ App f2 arg

cbvBigStepStrat = Strat { onApp , onAbs, onVar }
  where
    onApp _ f x = main (App f x)
    onAbs _ x b = main (Abs x b)
    onVar _ x   = main (Var x)
    main t = case helper t of
      Left  t2 -> trace (printf " -- %s" (show t2)) (main t2)
      Right t2 -> t2
    helper (Var x)     = -- trace "var" $
      Right $ Var x
    helper (Abs x b)   = -- trace "abs" $
      Right $ Abs x b
    helper (App (Abs x e) arg) =
      case helper arg of
        Left  arg2               -> Left  $ App (Abs  x e) arg
        Right arg2 | arg2 == arg -> Right $ subst (x,arg2) e
        Right arg2               -> Left  $ App (Abs x e) arg2
    helper (App f arg) = --trace "app" $
      case helper f of
        Left f2         ->        Left $ App f2 arg
        Right f2@(Abs x e) | f2 == f -> Right $ subst (x,arg) e
        Right f2@(Abs x e)           -> --trace ("4" ++ show f) $
          Left $ App f2 arg --subst (x,arg) e
        Right f2 | f2 == f -> Right $ (App f2 arg)
        Right f2        -> -- trace "3" $
          Right $ App f2 arg

a = Var "a"
x = Var "x"
y = Var "y"
z = Var "z"
f = Var "f"
g = Var "g"
h = Var "h"
m = Var "m"
n = Var "n"
p = Var "p"

zero  = Abs "f" $ Abs "x" $ x
one   = Abs "f" $ Abs "x" $ f `App` x
two   = Abs "f" $ Abs "x" $ f `App` (f `App` x)
three = Abs "f" $ Abs "x" $ f `App` (f `App` (f `App` x))

test strat term = do
  putStrLn $ printf "Evaluating: %s" (show term)
  rez <- return $ appStrat strat term
  putStrLn $ printf "Result:     %s\n" (show rez )
  return rez

testArithm = do
  test aoStrat $ zero
  test aoStrat $ one
  test aoStrat $ plus `App` one `App` one
  test aoStrat $ isZero `App` one `App` two `App` three
  where
    zero = Abs "g" $ Abs "y" $ Var "y"
    one  = Abs "f" $ Abs "x" $ (f `App` (Var "x"))
    two  = Abs "f" $ Abs "x" $ (f `App` (f `App` x))
    three= Abs "f" $ Abs "x" $ (f `App` (f `App` (f `App` x)))
    plus = Abs "m" $ Abs "n" $ Abs "f" $ Abs "x" $
            m `App` f `App` (n `App` f `App` x)
    mul = Abs "x" $ Abs "y" $ Abs "z" $
            App x (y `App` z)
    isZero = Abs "n" $ n `App` (Abs "x" false) `App` true
    true  = Abs "x" $ Abs "y" $ Var "x"
    false = Abs "x" $ Abs "y" $ Var "y"

testFacCBN = do
  -- test cbnBigStepStrat $ yCBN `App` fact `App` zero
  test cbnStrat $ mul `App` two `App` two
  test  aoStrat $ mul `App` two `App` two
  test cbnStrat $ yCBN `App` fact `App` two
  where
    fact = Abs "self" $ Abs "N" $
               isZero `App` (Var "N")
                `App` one
                `App` ((mul `App`
                           (Var "self" `App` (pred `App` (Var "N"))))
                           `App` (Var "N")
                      )
    zero = Abs "f" $ Abs "x" $ x
    one  = Abs "f" $ Abs "x" $ (f `App` (Var "x"))
    two  = Abs "f" $ Abs "x" $ (f `App` (f `App` x))
    -- (λxyz.x(yz))22 => (λz.2(2z)) => 4
    mul = Abs "x" $ Abs "y" $ Abs "z" $
          App x (y `App` z)
    isZero = Abs "n" $ n `App` (Abs "x" false) `App` true
    pred = Abs "n" $ Abs "f" $ Abs "x" $
                n `App` xxx `App` (Abs "u" x) `App` (Abs "u" $ Var "u")
          where xxx = Abs "g" $ Abs "h" $ h `App` (g `App` f)

    true  = Abs "x" $ Abs "y" $ Var "x"
    false = Abs "x" $ Abs "y" $ Var "y"
    yCBN = Abs "f" $ hack `App` hack
      where hack = Abs "x" $ f `App` (x `App` x)

testFacNO = do
  test norStrat $ pred `App` zero
  test norStrat $ sub `App` one `App` one
  test norStrat $ ite one one two
  test norStrat $ yCBN `App` fact `App` three
  return ()
  where
    fact = Abs "self" $ Abs "N" $
            ite (Var "N")
              one
              (mul  `App` (Var "self" `App` (pred `App` (Var "N")))
                    `App` (Var "N")
              )
    yCBN = Abs "f" $ hack `App` hack
      where hack = Abs "x" $ f `App` (x `App` x)
    -- (m+n) means get f, apply f m times, and after that apply f n times
    plus = Abs "m" $ Abs "n" $ Abs "f" $ Abs "x" $
           m `App` f `App` (n `App` f `App` x)
    mul = Abs "x" $ Abs "y" $ Abs "z" $ App x (y `App` z)
    pred = Abs "n" $ Abs "f" $ Abs "x" $
           n `App` xxx `App` (Abs "u" x) `App` (Abs "u" $ Var "u")
      where xxx = Abs "g" $ Abs "h" $ h `App` (g `App` f)
    sub = Abs "m" $ Abs "n" $ n `App` pred `App` (Var "m")
    isZero = Abs "x" $ x `App` false `App` neg `App` false
    true  = Abs "x" $ Abs "y" $ Var "x"
    false = Abs "x" $ Abs "y" $ Var "y"
    neg   = Abs "x" $ x `App` false `App` true
    ite cond th el = isZero `App` cond `App` th `App` el


-- Demos with call-by-value
testFacCBV = do
  -- test cbnBigStepStrat $ yCBN `App` fact `App` zero
  -- temp0 <- test cbvStrat $ mul `App` one `App` two
  -- test aoStrat temp0

  -- test  aoStrat $ plus `App` two `App` two
  temp1 <- test cbvStrat $ plus `App` one `App` one
  test aoStrat temp1
  -- temp2 <- test cbvBigStepStrat $ plus1 `App` two
  -- test aoStrat temp2
  -- _ <- test cbvBigStepStrat $ testBadITE (plus1 `App` one) zero `App` zero
  temp3 <- test cbvBigStepStrat $ (plus1 `App` one)
  test aoStrat temp3
  -- Next will hang
  -- test cbvBigStepStrat $ testBadITE `App` zero
  -- test cbnStrat $ testBadITE two selfOmega `App` zero
  -- iteCBV doesn't hang
  test cbvStrat $ iteCBV zero two selfOmega
  -- recursive sum using fixpoint
  tempSum <- test cbvStrat $ yCBV `App` sum `App` three
  test aoStrat tempSum
  -- tempFact <- test cbvStrat $ yCBV `App` fact `App` three
  -- test aoStrat tempFact
  return ()
  where
    fact = Abs "self" $ Abs "N" $
                iteCBV (Var "N")
                  one
                  ((mul `App` (Var "self" `App` (pred `App` (Var "N"))))
                        `App` (Var "N")
                  )
    sum = Abs "self" $ Abs "N" $
          (isZero `App` (Var "N")
          `App` (Abs "tmp" zero)
          `App` (Abs "tmp" $
                 plus `App` (Var "N") `App`
                        (Var "self" `App` (pred `App` (Var "N")))
                )
          ) `App` (Var "tmp")
    testITE =
      Abs "N" $
          (isZero
          `App` (Var "N")
          `App` (Abs "tmp" two)
          `App` (Abs "tmp" $ selfOmega)
          ) `App` (Var "tmp")
    testBadITE thB elseB =
      Abs "N" $
          (isZero
          `App` (Var "N")
          `App` thB
          `App` elseB
          )

    iteCBV cond thB elsB =
        (isZero
          `App` cond
          `App` (Abs fresh $ thB)
          `App` (Abs fresh $ elsB)
        ) `App` (Var fresh)
      where
        fresh = nextName "fresh" $ freeVars thB ++ freeVars cond ++ freeVars elsB


    mul = Abs "x" $ Abs "y" $ Abs "z" $ App x (y `App` z)
    -- (m+n) means get f, apply f m times, and after that apply f n times
    plus = Abs "m" $ Abs "n" $ Abs "f" $ Abs "x" $
           m `App` f `App` (n `App` f `App` x)
    plus1 = Abs "n" $ Abs "f" $ Abs "x" $
            f `App`
            (n `App` f `App` x)
    isZero = Abs "n" $ n `App` (Abs "x" false) `App` true
    pred = Abs "n" $ Abs "f" $ Abs "x" $
                n `App` xxx `App` (Abs "u" x) `App` (Abs "u" $ Var "u")
          where xxx = Abs "g" $ Abs "h" $ h `App` (g `App` f)

    true  = Abs "x" $ Abs "y" $ Var "x"
    false = Abs "x" $ Abs "y" $ Var "y"
    -- Yv ≡ λh.(λx.λa.h (x x) a) (λx.λa.h (x x) a)
    -- a.k.a Zed combinator
    yCBV = Abs "f" $ hack `App` hack
      where hack = Abs "x" $ Abs "a" $ f `App` (x `App` x) `App` a
    omega = Abs "x" (x `App` x)
    selfOmega = omega `App` omega

-- bigStep and small-step CNB and CBN
bigVsSmallStep = do
  putStrLn "Small-step CBN"
  test cbnStrat $ input
  putStrLn "Small-step CBV"
  test cbvStrat $ input

  putStrLn "Big step CBN"
  test cbnBigStepStrat $ input
  putStrLn "Big step CBV"
  test cbvBigStepStrat $ input
  where
    input = (Abs "x" $ Abs "w" $ App y x `App` x)
            `App` ((Abs "z" $ z) `App` (Var "z"))
            `App` ((Abs "v" $ m) `App` m)

main :: IO ()
main = do
  let input = (Abs "x" $ Abs "w" $ App y x `App` x)
              `App` ((Abs "z" $ z) `App` (Var "z"))
              `App` ((Abs "v" $ m) `App` m)
  putStrLn "Small-step CBN"
  test cbnStrat $ input
  putStrLn "Small-step CBV"
  test cbvStrat $ input

  putStrLn "Big step CBN"
  test cbnBigStepStrat $ input
  putStrLn "Big step CBV"
  test cbvBigStepStrat $ input
  -- test cbnBigStepStrat $  App (Abs "x" $ Abs "y" (Var "x")) (Var "z")
  -- test cbnBigStepStrat $  add
  -- test cbnBigStepStrat $  four
  -- test  cbnBigStepStrat  $ App (App add one) zero
  -- facAO
  -- test cbnBigStepStrat $  Abs "x" $ Abs "y" (Var "x")
  return ()
  where
    facAO = do
      -- test aoStrat $ fact
      -- test aoStrat $ neg `App` true
      -- test aoStrat $ isZero `App` one
      -- test aoStrat $ isZero `App` (minus `App` two `App` two) `App` two `App` one
      -- test cbnStrat $ fact `App` three
      -- test cbnBigStepStrat $ yCBN `App` (Abs "self" sum) `App` one
      -- test cbnStrat $ yCBN `App` (Abs "self" fact) `App` two
      -- test aoStrat $ yAO `App` sum `App` zero
      -- test cbnStrat $ Abs "N" $  isZero `App` zero `App` one `App` omega
      -- test cbnStrat $ yCBN `App` (Abs "self" fact) `App` two `App` plus1 `App` zero
      -- test cbnStrat $ yCBN `App` sum `App` two  `App` plus1 `App` zero
      -- test cbnBigStepStrat $ yCBN `App` (Abs "self" $ plus1 `App` one)
      -- test cbnBigStepStrat $ yCBN `App` demo1 `App` two
      -- test cbnBigStepStrat $ pred `App` zero
      -- test aoStrat $ pred `App` zero
      -- test aoStrat $ isZeroAO `App` zero `App` ( one) `App` one
      test cbnBigStepStrat $ yCBN `App` demo1 `App` two
      -- test cbvStrat $ yCBV `App` demo1 `App` two
      -- print phi
      where
        isZeroAO = Abs "n" $ n `App` (Abs "x" false) `App` true
        demo1 = Abs "self" $  Abs "N" $
                isZeroAO `App` (Var "N")
                `App` zero
                `App` (succ `App`
                          (Var "self" `App` (pred `App` (Var "N")))
                      )

        fact = Abs "self" $ Abs "N" $
               isZero `App` (Var "N")
                `App` one
                `App` (mul `App`
                           (Var "self" `App` (pred `App` (Var "N")))
                           `App` (Var "N")
                      )
        sum = Abs "self" $ Abs "N" $
              isZero `App` (Var "N")
              `App` zero
              `App` (plus1 `App`
                           (Var "self" `App` (pred `App` (Var "N")))
                    )
        omega = Abs "x" (x `App` x)
        selfOmega = omega `App` omega
        -- Yn ≡ λh.(λx.f (x x)) (λx.f (x x))
        yCBN = Abs "f" $ hack `App` hack
          where hack = Abs "x" $ f `App` (x `App` x)
        mul = Abs "x" $ Abs "y" $ Abs "z" $
              App x (y `App` z)
        plus = Abs "m" $ Abs "n" $ Abs "f" $ Abs "x" $
               m `App` f `App` (n `App` f `App` x)
        plus1 = plus `App` one
        zero = Abs "f" $ Abs "x" $ Var "x"
        one  = Abs "f" $ Abs "x" $ (Var "f" `App` (Var "x"))
        true  = Abs "x" $ Abs "y" $ Var "x"
        false = Abs "x" $ Abs "y" $ Var "y"
        neg   = Abs "x" $ x `App` false `App` true
        isZero = Abs "x" $ (Var "x") `App` false `App` neg `App` false
        pair = Abs "a" $ Abs "b" $ Abs "z" $ (Var "z") `App` (Var "a") `App` (Var "b")
        succ = Abs "w" $ Abs "y" $ Abs "x" $
               y `App` (Var "w" `App` y `App` x)
        pred = Abs "n" $ Abs "f" $ Abs "x" $
                n `App` xxx `App` (Abs "u" x) `App` (Abs "u" $ Var "u")
          where xxx = Abs "g" $ Abs "h" $ h `App` (g `App` f)
        minus = Abs "m" $ Abs "n" $ n `App` pred `App` (Var "m")
        -- minus1 = Abs "m" $ minus `App` one

    -- Yv ≡ λh.(λx.λa.h (x x) a) (λx.λa.h (x x) a)
    -- the same for applicative fixpoint
    yCBV = Abs "f" $ hack `App` hack
      where hack = Abs "x" $ Abs "a" $ f `App` (x `App` x) `App` a
    yAO = Abs "f" $ hack `App` hack
      where hack = Abs "x" $ f `App` (Abs "a" $ (x `App` x) `App` a)


    -- zed = Abs "f" $ hack `App` hack
    --   where hack = Abs "x" $ Abs "a" $ (Var "h") `App` ((Var "x")`App`(Var "x")) `App` (Var "a")
    zero = Abs "g" $ Abs "y" $ Var "y"
    one  = Abs "f" $ Abs "x" $ (f `App` (Var "x"))
    two  = Abs "f" $ Abs "x" $ (f `App` (f `App` x))
    three= Abs "f" $ Abs "x" $ (f `App` (f `App` (f `App` x)))
    four = Abs "f" $ Abs "x" $ (Var "f" `App` (Var "f" `App` (Var "f" `App` Var "x")))
    add  = Abs "m" $ Abs "n" $ Abs "f" $ Abs "x" $
            (Var "m" `App` Var "f" `App` (Var "n" `App` Var "f" `App` Var "x"))
