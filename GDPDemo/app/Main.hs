module Main where

import Sorted
import Data.The
import Theory.Named

main :: IO ()
main = do
  xs <- readLn :: IO [Int]
  ys <- readLn
  name compare $ \gt -> do
    let xs' = sortBy gt xs
        ys' = sortBy gt ys
    print (the (mergeBy gt xs' ys'))
