## 6 implementaions of expression tree evaluator

From 1 to 6 we increase quality of functional code.

Compilation:

```
 stack build && stack exec Expr6-exe
```

ghcid startup command (where 1 can be in 1..6)

```
stack exec ghcid -- app1/Main.hs --test='main'
```
