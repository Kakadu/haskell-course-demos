{-# LANGUAGE ScopedTypeVariables #-}
{- # LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Main where

data Expr =
    Num Int
  | Plus Expr Expr
  | Mul Expr Expr
  | Neg Expr
  | Ident String
  deriving Show

-- introduce fmap via bind
eval :: (String -> Maybe Int) -> Expr -> Maybe Int
eval find = helper
  where
    return :: a -> Maybe a
    return = Just

    fmap :: (a -> b) -> Maybe a -> Maybe b
    fmap f e =
      e >>= (\x -> return (f x))

    e >>= f = case e of
      Nothing -> Nothing
      Just x -> f x

    helper :: Expr -> Maybe Int
    helper (Num n) = return n
    helper (Ident s) = find s

    helper (Neg e) = fmap (\x -> -x) (helper e)

    helper (Plus l r) =
      helper l >>= \l ->
      helper r >>= \r ->
      return (l+r)

    helper (Mul l r) =
      helper l >>= \l ->
      helper r >>= \r ->
      return (l*r)

main :: IO ()
main = do
  test []                    (Plus (Num 1) (Num 2))
  test [("a", 5)]            (Plus (Ident "a") (Num 2))
  test [("a", 5), ("b", 10)] (Plus (Ident "a") (Neg (Ident "b")))
  where
    test env e = do
      putStr (show e)
      putStr " --> "
      print (eval (\s -> lookup s env) e)

-- next: big refactoring
