{-# LANGUAGE ScopedTypeVariables #-}
{- # LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}

module Main where

-- First implementation.
-- TODO: say about type inferece

data Expr =
    Num Int
  | Plus Expr Expr
  | Mul Expr Expr
  | Neg Expr
  | Ident String
  deriving Show

eval :: (String -> Maybe Int) -> Expr -> Maybe Int
eval find (Num n) = Just n
eval find (Ident s) = find s
eval find (Neg e) =
  case eval find e of
    Nothing -> Nothing
    Just l -> Just (-l)

eval find (Plus l r) =
  case eval find l of
    Nothing -> Nothing
    Just l ->
      case eval find r of
        Nothing -> Nothing
        Just r ->  Just (l+r)

eval find (Mul l r) =
  case eval find l of
    Nothing -> Nothing
    Just l ->
      case eval find r of
        Nothing -> Nothing
        Just r ->  Just (l*r)

main :: IO ()
main = do
  test []                    (Plus (Num 1) (Num 2))
  test [("a", 5)]            (Plus (Ident "a") (Num 2))
  test [("a", 5), ("b", 10)] (Plus (Ident "a") (Neg (Ident "b")))
  where
    test :: [(String, Int)] -> Expr -> IO ()
    test env e = do
      putStr (show e)
      putStr " --> "
      print (eval (\s -> lookup s env) e)

{- Next step: Small refactoring with inner function inside `eval`
-}
