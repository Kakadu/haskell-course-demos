{-# LANGUAGE ScopedTypeVariables #-}
{- # LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}
module Main where

-- introduce monads
-- simplified typeclasses are very similar to interfaces

data Expr =
    Num Int
  | Plus Expr Expr
  | Mul Expr Expr
  | Neg Expr
  | Ident String
  deriving Show

eval :: Monad m => (String -> m Int) -> Expr -> m Int
eval find = helper
  where
    one = return

    helper (Num n) = one n
    helper (Ident s) = find s

    helper (Neg e) = fmap (\x -> -x) (helper e)

    helper (Plus l r) =
      helper l >>= \l ->
      helper r >>= \r ->
      one (l+r)

    helper (Mul l r) =
      helper l >>= \l ->
      helper r >>= \r ->
      one (l*r)

main :: IO ()
main = do
  test []                     (Plus (Num 1) (Num 2))
  test [("a", 5)]             (Plus (Ident "a") (Num 2))
  test [("a", 5), ("b", 10)]  (Plus (Ident "a") (Neg (Ident "b")))
  test [("a", 1), ("b", 100)] (Plus (Ident "a") (Neg (Ident "b")))
  test [("a", 1), ("b", 1)]   (Plus (Ident "a") (Neg (Ident "b")))

  where
    test env expr = do
      test1 env expr
      test2 env expr
    test1 env expr = do
      putStr (show expr)
      putStr " --> "
      print (eval (\name -> lookup name env) expr)
    test2 env expr = do
      putStr (show expr)
      putStr " --> "
      print (eval (\s -> lookup2 s env) expr)

    lookup2 :: String -> [(String,Int)] -> [Int]
    lookup2 name env = case lookup name env of
      Just x -> [x, -x]
      Nothing -> []
