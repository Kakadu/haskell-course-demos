{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExplicitForAll, TypeSynonymInstances, InstanceSigs #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}

module Main where

  {- Writer monad demo
  TODO: Milewski's example?
   -}
import Text.Printf
import Control.Monad

-- Example 0
testEvalNaive = do
  putStrLn "No writer monad"
  print $ eval (Con 10)
  print $ eval (Con 10 `Div` Con 2)
  where
    -- Evaluate and count divisions
    eval :: Term -> (Int, String)
    eval (Con x) = (x, printf "(const %d)" x)
    eval (Div l r) =
      let (a, log1) = eval l in
      let (b, log2) = eval r in
      let log3 = printf "%s %s (div %d %d)" log1 log2 a b in
      (a `div` b, log3)


data MyWriterMonad a = W { unW :: (String, a) }

instance Show a => Show (MyWriterMonad a) where
  show (W (s,a)) = printf "(%s,\"%s\")" (show a) s

out :: String -> MyWriterMonad ()
out s = W (s, ())

instance Monad MyWriterMonad where
  return :: forall a . a -> MyWriterMonad a
  return x = W ("", x)

  (>>=) :: forall a b . MyWriterMonad a -> (a -> MyWriterMonad b) -> MyWriterMonad b
  W (s1,a) >>= f =  W (printf "%s%s" s1 s2, b)
                    where (s2, b) = unW (f a)


instance Applicative MyWriterMonad where
  pure = return
  f <*> x = f >>= \f -> x >>= \x -> return $ f x
instance Functor     MyWriterMonad where
  fmap f x = x >>= \x -> return $ f x

-- Example 1
data Term = Con Int | Div Term Term

eval :: Term -> MyWriterMonad Int
eval (Con a)  = do
  out $ printf "(const %d) " a
  return a
eval (Div l r)  = do
  a <- eval l
  b <- eval r
  out $ printf "(div %d %d) " a b
  return (a `div` b)

testEval = do
  putStrLn "testing evaluation"
  print $  (eval (Con 10))
  print $  (eval (Con 10 `Div` Con 2))


-- -- Example 2
-- type GameValue = Int
-- type GameState = (Bool, Int)

-- playGame :: String -> MyStateMonad GameState GameValue
-- playGame []     = do
--     (_, score) <- get
--     return score

-- playGame (x:xs) = do
--     (on, score) <- get
--     case x of
--          'a' | on -> put (on, score + 1)
--          'b' | on -> put (on, score - 1)
--          'c'      -> put (not on, score)
--          _        -> put (on, score)
--     playGame xs

-- startState = (False, 0)
-- testGame = do
--   putStrLn "Testing game"
--   print $ runState (playGame "abcaaacbbcabbab") startState

main = do
  testEvalNaive
  testEval
  -- testGame
