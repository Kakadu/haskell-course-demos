{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExplicitForAll, TypeSynonymInstances, InstanceSigs #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}

module Main where

  {-  State monad definition.
    It's use in evaluator of arithmetic expressions
    It's use in mini-game
  -}

import Control.Monad

data MyStateMonad s a = ST { unST :: s -> (a, s) }

runState :: MyStateMonad s a -> s -> (a, s)
runState st init = unST st init

get :: forall s . MyStateMonad s s
get = ST (\s -> (s,s))
put :: forall s . s  -> MyStateMonad s ()
put s = ST $ \_ -> ((), s)

instance Monad (MyStateMonad s) where
  return :: forall a . a -> MyStateMonad s a
  return x = ST (\st -> (x, st))

  (>>=) :: forall a b . MyStateMonad s a -> (a -> MyStateMonad s b) -> MyStateMonad s b
  ST g >>= f = ST $  \st0 ->
        let (ans1,st1) = g st0 in
        runState (f ans1) st1

instance Applicative (MyStateMonad s) where
  pure = return
  f <*> x = f >>= \f -> x >>= \x -> return $ f x
instance Functor     (MyStateMonad s) where
  fmap f x = x >>= \x -> return $ f x

-- Example 0
testEvalNaive = do
  putStrLn "No explicit state monad"
  print $ eval (Con 10) 111
  print $ eval (Con 10 `Div` Con 2) 111
  where
    -- Evaluate and count divisions
    eval :: Term -> Int -> (Int,Int)
    eval (Con a) x = (a, x )
    eval (Div l r) x =
      let (a, y) = eval l x in
      let (b, z) = eval r y in
      (a `div` b, z + 1)


-- Example 1
data Term = Con Int | Div Term Term

eval :: Term -> MyStateMonad Int Int
eval (Con a)  = return a
eval (Div l r)  = do
  a <- eval l
  b <- eval r
  old <- get
  put (1+old)
  return (a `div` b)

testEval = do
  putStrLn "testing evaluation"
  print $ runState (eval (Con 10)) 111
  print $ runState (eval (Con 10 `Div` Con 2)) 111


-- Example 2
type GameValue = Int
type GameState = (Bool, Int)

playGame :: String -> MyStateMonad GameState GameValue
playGame []     = do
    (_, score) <- get
    return score

playGame (x:xs) = do
    (on, score) <- get
    case x of
         'a' | on -> put (on, score + 1)
         'b' | on -> put (on, score - 1)
         'c'      -> put (not on, score)
         _        -> put (on, score)
    playGame xs

startState = (False, 0)
testGame = do
  putStrLn "Testing game"
  print $ runState (playGame "abcaaacbbcabbab") startState

main = do
  testEvalNaive
  testEval
  testGame
