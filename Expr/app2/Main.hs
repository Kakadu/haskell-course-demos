{-# LANGUAGE ScopedTypeVariables #-}
{- # LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}

module Main where

-- inner functions are not popular in mainstream programming.
-- Compiler is smart enough.
data Expr =
    Num Int
  | Plus Expr Expr
  | Mul Expr Expr
  | Neg Expr
  | Ident String
  deriving Show

eval :: (String -> Maybe Int) -> Expr -> Maybe Int
eval find = helper
  where
    helper :: Expr -> Maybe Int
    helper (Num n) = Just n
    helper (Ident s) = find s
    helper (Neg e) =
      case helper e of
        Nothing -> Nothing
        Just l -> Just (-l)

    helper (Plus l r) =
      case helper l of
        Nothing -> Nothing
        Just l ->
          case helper r of
            Nothing -> Nothing
            Just r ->  Just (l+r)

    helper (Mul l r) =
      case helper l of
        Nothing -> Nothing
        Just l ->
          case helper r of
            Nothing -> Nothing
            Just r ->  Just (l*r)

main :: IO ()
main = do
  test []                    (Plus (Num 1) (Num 2))
  test [("a", 5)]            (Plus (Ident "a") (Num 2))
  test [("a", 5), ("b", 10)] (Plus (Ident "a") (Neg (Ident "b")))
  where
    test env e = do
      putStr (show e)
      putStr " --> "
      print (eval (\s -> lookup s env) e)

{- Next step: introduce return and >>=
-}
