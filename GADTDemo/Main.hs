{-# LANGUAGE ExplicitForAll #-}
{-# OPTIONS_GHC -Wall -Wno-unused-imports #-}
module Main where

import Prelude hiding (head)
import qualified ExprSmart (test1)
-- import Lib
import NonemptyList

-- totalHd :: forall a . Plist a -> Int
-- totalHd Nil = 42
-- totalHd xs@(Cons _ _)  = head (xs :: Plist a) -- no error !


main :: IO ()
main = do
  print ExprSmart.test1
  return ()
