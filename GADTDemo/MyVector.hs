{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE  GADTs, NoImplicitPrelude #-}

module MyVector where

import Prelude (Int)

data Empty
data Nonempty

data List prop where
  Nil :: List Empty
  Cons :: Int -> List prop  -> List Nonempty

{-
Like smart constructors of phantom types, this declarationrestricts the typeof constructed values with respect to the data constructors they are builtwith.

In addition, the fact that specific types are now attached to data constructorsand not to smart constructors, which are regular values, yieldsa more precisetyping of pattern matching branches.
-}


hd :: List Nonempty -> Int
hd (Cons h _) = h

{-
 the type-checker will not complain anymore that the pattern matching is notexhaustive.

 Indeed, it is capable of proving that the unspecified case corresponding to the pattern `Nil` is impossible given that such a pattern would only capture values oftype `List Empty`, which is incompatible withn `List Nonempty`.
-}
totalHd :: List a -> Int
totalHd Nil = 42
totalHd xs@(Cons _ _)  = hd xs
-- totalHd xs  = hd xs -- doesn't compile

{-
Besides, a type-checker for GADTs is able torefine the type oflin thesecond case of this pattern matching:

In the body of the last branch, the type-checker knows that theterm xs has both the type `List a` and the type `List Nonempty` because `xs` has been built by application of the data constructor `Cons`. The second type is enough to accept the application of hd on `xs`.
-}


{-
Which technical device is used by the type-checkerto assign several types to one particular term?

We can morally reformulate our previous GADT definition by attaching a type equality to each data constructor:
-}

data List2 prop where
  Nil2 :: prop ~ Empty => List2 prop
  Cons2 :: p ~ Nonempty => Int -> List prop  -> List2 p

{-
Local type equalities
In the right-hand side of a pattern matching branch corresponding to aparticular data constructor, the associated type equalities areimplicitlyassumedby the type-checker.
A conversion rule allows type assignment modulo these type equalities.
-}
