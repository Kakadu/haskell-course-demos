{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE GADTs #-}

module Lib where

import Control.Monad

-- GADT representation.
-- Simply-typed lambda calculus with de Bruijn indices,
-- with integer constants, and addition.
-- Philip Wadler and Shayan Najd, November 2013

data Exp e a where
  Con :: Int -> Exp e Int
  Add :: Exp e Int -> Exp e Int -> Exp e Int
  Var :: Var e a -> Exp e a
  Abs :: Typ a -> Exp (e,a) b -> Exp e (a -> b)
  App :: Exp e (a -> b) -> Exp e a -> Exp e b

-- Types (Singleton)
data Typ a where
  Int :: Typ Int
  Arr :: Typ a -> Typ b -> Typ (a -> b)

-- Environment (Singleton)
data Env e where
  Emp :: Env ()
  Ext :: Env e -> Typ a -> Env (e,a)

  -- Type and environment equality

data Equal a b where
  Eq :: Equal a a

eqTyp :: Typ a -> Typ a' -> Maybe (Equal a a')
eqTyp Int Int                =  return Eq
eqTyp (Arr t u) (Arr t' u')  =  do Eq <- eqTyp t t'
                                   Eq <- eqTyp u u'
                                   return Eq
eqTyp _ _                    =  mzero

eqEnv :: Env e -> Env e' -> Maybe (Equal e e')
eqEnv Emp Emp                =  return Eq
eqEnv (Ext e a) (Ext e' a')  =  do Eq <- eqEnv e e'
                                   Eq <- eqTyp a a'
                                   return Eq
eqEnv _ _                    =  mzero

-- Variables
data Var e a where
  Zro :: Var (e,a) a
  Suc :: Var e a -> Var (e,b) a


-- Extraction of values form environment
get :: Var e a -> e -> a
get Zro     (_ ,x)      = x
get (Suc n) (xs,_)      = get n xs

-- Extraction of values form environment with singletons
gets :: Var e a -> Env e -> Typ a
gets Zro     (Ext _  x) = x
gets (Suc n) (Ext xs _) = gets n xs
-- gets _       Emp        = error "Impossible!"

-- Evaluation of expressions under specific environment of values
eval :: Exp e a -> e -> a
eval (Con i)     _ = i
eval (Var x)     r = get x r
eval (Abs _  eb) r = \v -> eval eb (r,v)
eval (App ef ea) r = eval ef r $ eval ea r
eval (Add el er) r = eval el r + eval er r

-- Typechecking and returning the type, if successful
chk :: Exp e a -> Env e -> Typ a
chk (Con _)     _ = Int
chk (Var x)     r = gets x r
chk (Abs ta eb) r = ta `Arr` chk eb (r `Ext` ta)
chk (App ef _ ) r = case chk ef r of
  Arr _ tr -> tr
chk (Add _  _ ) _ = Int

infer :: Exp e a -> Env e -> Maybe (Typ a)
infer (Con _) _     =  return Int
infer (Add m n) r   =  do Int <- infer m r
                          Int <- infer n r
                          return Int
infer (Var x) r      =  return (gets x r)
infer (Abs t n) r    =  do u <- infer n (Ext r t)
                           return (Arr t u)
infer (App l m) r    =  do Arr t u <- infer l r
                           t' <- infer m r
                           Eq <- eqTyp t t'
                           return u

-- An example expression doubling the input number
dbl :: Exp () (Int -> Int)
dbl = Abs Int (Var Zro `Add` Var Zro)

-- An example expression composing two types
compose :: Typ a -> Typ b -> Typ c -> Exp () ((b -> c) -> (a -> b) -> (a -> c))
compose s t u = Abs (t `Arr` u) (Abs (s `Arr` t) (Abs s
                  (Var (Suc (Suc Zro)) `App` (Var (Suc Zro) `App` Var Zro))))

-- An example expression representing the Integer 4
four :: Exp () Int
four = (compose Int Int Int `App` dbl `App` dbl) `App` (Con 1)

-- Two test cases
test :: Bool
test = (case chk four Emp of
          Int -> True)
        &&
        (eval four () == 4)
