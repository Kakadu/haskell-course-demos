{-# OPTIONS_GHC -Wall -Wno-unused-top-binds -Wincomplete-patterns #-}
-- {-# LANGUAGE ExplicitForAll #-}

module NonemptyList
  (ListInt(..), Plist, cons, nil, head)
  where

import Prelude hiding (head)

data ListInt =
    Nil
  | Cons Int ListInt

hd :: ListInt -> Int
hd Nil        = {- List is empty      -} error "empty list"
hd (Cons x _) = {- List is non-empty  -} x

newtype Plist info = L ListInt

data Empty
data Nonempty

nil :: Plist Empty
nil = L Nil

cons :: Int -> Plist b -> Plist Nonempty
cons x (L xs) = L $ Cons x xs

head :: Plist Nonempty -> Int
head (L (Cons x _ )) = x
head (L Nil)         = error "should not happen"

totalHd :: Plist a -> Int
totalHd (L Nil) = 42
{-

NonemptyList.hs:34:1: warning: [-Wincomplete-patterns]
    Pattern match(es) are non-exhaustive
    In an equation for ‘totalHd’: Patterns not matched: (L (Cons _ _))
   |
34 | totalHd (L Nil) = 42
   | ^^^^^^^^^^^^^^^^^^^^

-}



-- totalHd xs@(L (Cons _ _))  = head xs

{-
NonemptyList.hs:35:35: error:
    • Couldn't match type ‘a’ with ‘Nonempty’
      ‘a’ is a rigid type variable bound by
        the type signature for:
          totalHd :: forall a. Plist a -> Int
        at NonemptyList.hs:33:1-25
      Expected type: Plist Nonempty
        Actual type: Plist a
    • In the first argument of ‘head’, namely ‘xs’
      In the expression: head xs
      In an equation for ‘totalHd’: totalHd xs@(L (Cons _ _)) = head xs
    • Relevant bindings include
        xs :: Plist a (bound at NonemptyList.hs:35:9)
        totalHd :: Plist a -> Int (bound at NonemptyList.hs:34:1)
   |
35 | totalHd xs@(L (Cons _ _))  = head xs  -- no error !
   |
-}
_ = totalHd $ cons 5555 nil

-- main :: IO ()
-- main  = return ()
