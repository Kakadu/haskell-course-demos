{-# OPTIONS_GHC -Wall -Wno-unused-top-binds #-}
{-# LANGUAGE ExplicitForAll, GADTs #-}
module ExprSmart where

import Control.Exception (assert)

data Term a where
  Const :: Int -> Term Int
  Pair :: Term a ->  Term b -> Term (a,b)
  Fst :: Term (a,b) -> Term a
  Snd :: Term (a,b) -> Term b

type Value a = a

eval :: Term a -> Value a
eval (Const n) = n
eval (Pair l r) = (eval l, eval r)
eval (Fst t) = fst $ eval t
eval (Snd t) = snd $ eval t

{- How to convince ourselves that well-typed expressions do not go wrong?
-}

test1 :: Bool
test1 = assert (eval (Fst $ Pair (Const 1) $ Const 2) == 1) True
