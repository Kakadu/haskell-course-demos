module ExprNaive where

data Term =
    Const Int
  | Pair Term Term
  | Fst Term
  | Snd Term

data Value = VInt Int | VPair Value Value

eval :: Term -> Value
eval (Const n) = VInt n
eval (Pair  l r) = VPair (eval l) (eval r)
eval (Fst t) = case eval t of
  VInt _ -> error "only pairs can be projected"
  VPair a _ -> a

eval (Snd t) = case eval t of
  VInt _ -> error "only pairs can be projected"
  VPair _ b -> b

{- How to convince ourselves that well-typed expressions do not go wrong?
-}

_ = seq (eval (Fst $ Pair (Const 1) $ Const 2)) ()
